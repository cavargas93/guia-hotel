module.exports = function (grunt){

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: { //Browser files
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' //Directorio base para nuestro servidor
                    }
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'images/*.{png,gif,jpg,jpeg}',
                    dest: 'dist/'
                }]
            }
        },

        copy: { // pasa los archivos html a dest
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            }
        },

        clean: { // limpia la carpeta dist
            build: {
                src: ['dist/']
            }
        },

        cssmin: {
            dist: {}
        },

        uglify: {
            dist: {}
        },

        filerev: {
            // Pone el codigo hash md5 a lo ultimo de los archivos css y js para 
            // Versionar con cadena hash, permite identificar cada generacion de un archivo de otro, esto permite que cuando un archivo cambie en produccion obligue al browser a no usar la cache sino a descargar el nuevo archivo, ese cambio con hash indica al browser que lo tiene que descargar 
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release: {
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css'
                    ]
                }]
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },

        useminPrepare: {// toma los html y le aplica cssmin y uglify, pa luego guardarlos en dist
            foo: {
                dest: 'dist',
                src: ['index.html', 'about.html', 'contact.html', 'price.html', 'terms.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block){
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        usemin: {
            html: ['dist/index.html', 'dist/about.html', 'dist/contact.html', 'dist/price.html', 'dist/terms.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }
        }
    });
    
    /* grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-imagemin'); */

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    const newLocal = 'img:compress';
    grunt.registerTask(newLocal, ['imagemin']);
    grunt.registerTask('build', [
        'clean', // eliminar carpeta disr
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin', // Crea el archivo index.css de la carpeta dist
        'uglify', // Crea el archivo main.js de la carpeta dist
        'filerev', // Agrega cadena versionando con codigo hash, hace que los archivos no sean cacheables los archivos que generamos
        'usemin'
    ])
};