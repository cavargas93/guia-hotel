$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 5000
    });

    $("#contactoModal").on('shown.bs.modal', function(e){
      console.log("El modal se mostró")
    });

    $("#contactoModal").on('show.bs.modal', function(e){
      console.log("El modal se muestra");
      $('#contactoBtn').removeClass('btn-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);
    });

    $("#contactoModal").on('hide.bs.modal', function(e){
      console.log("El modal se ocultó")
    });

    $("#contactoModal").on('hidden.bs.modal', function(e){
      console.log("El modal se oculta");
      $('#contactoBtn').prop('disabled', false);
    });
  });